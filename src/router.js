import Vue from 'vue';
import Router from 'vue-router';
import queryString from 'query-string';
import store from './store';
import Home from './views/Home.vue';
import Login from './views/Login.vue';
import Thesis from './views/Thesis.vue';
import Add from './views/Add.vue';
import Overview from './views/Overview';
import Toggle from './views/Toggle';
import List from './views/List';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/thesis',
      name: 'thesis',
      component: Thesis,
      meta: {
        requiresRoles: ['student']
      }
    },
    {
      path: '/list',
      name: 'list',
      component: List,
      meta: {
        requiresRoles: ['teacher']
      }
    },
    {
      path: '/overview',
      name: 'overview',
      component: Overview,
      meta: {
        requiresRoles: ['admin']
      }
    },
    {
      path: '/add',
      name: 'add',
      component: Add,
      meta: {
        requiresRoles: ['admin']
      }
    },
    {
      path: '/toggle',
      name: 'toggle',
      component: Toggle,
      meta: {
        requiresRoles: ['admin']
      }
    }
  ]
});

// Redirect to log in screen if trying to access route that shouldn't be accessible
router.beforeEach((to, from, next) => {
  const { userRole } = store.getters;

  const hasRole = to.matched.every(
    ({ meta: { requiresRoles } }) =>
      !requiresRoles || (requiresRoles && requiresRoles.includes(userRole))
  );

  if (!userRole && !hasRole) {
    const query = queryString.stringify({ redirect: to.fullPath });
    return next(`/login?${query}`);
  }

  if (!hasRole) {
    return next(from.path);
  }

  return next();
});

export default router;
