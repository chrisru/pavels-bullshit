const parseJWT = token => {
  if (!token) {
    return null;
  }

  try {
    return JSON.parse(window.atob(token.split('.')[1]));
  } catch (error) {
    return null;
  }
};

const initialState = () => {
  const token = window.localStorage.getItem('jwt');

  return {
    jwt: token,
    parsedJWT: parseJWT(token),
    isAuthenticated: false
  };
};

const authModule = {
  state: initialState(),

  mutations: {
    login(state, token) {
      state.jwt = token;
      state.parsedJWT = parseJWT(token);
      state.isAuthenticated = true;
      window.localStorage.setItem('jwt', token);
    },

    logout(state) {
      state.jwt = null;
      state.parsedJWT = null;
      state.isAuthenticated = false;
      window.localStorage.removeItem('jwt');
    },

    setAuthenticated(state, authenticated) {
      state.isAuthenticated = authenticated;
    }
  },

  getters: {
    userRole: state => {
      if (state.parsedJWT) {
        return state.parsedJWT.user.role;
      }

      return null;
    }
  }
};

export default authModule;
