import queryString from 'query-string';

const Account = {
  methods: {
    redirect() {
      const { redirect = '/' } = queryString.parse(window.location.search);
      this.$router.push(redirect);
    }
  }
};

export default Account;
