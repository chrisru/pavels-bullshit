import axios from 'axios';
import store from './store';

const config = {
  api: process.env.VUE_APP_API_URL
};

const getClient = () => {
  return axios.create({
    baseURL: config.api,
    headers: {
      Authorization: `Bearer ${store.state.auth.jwt}`
    }
  });
};

export const fetchIsAuthenticated = () => {
  return getClient().get(`/authenticated`);
};

export const postLogin = (username, password) => {
  return getClient().post(`/auth`, {
    username,
    password
  });
};
